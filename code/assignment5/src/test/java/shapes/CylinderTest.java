/**
 * CylinderTest is a class used for testing all the methods in the Cylinder class.
 * @author Sungeun Kim
 * @studentId 2042714
 * @version 10/3/2023
 */

package shapes;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {
    /**
     * This method tests the constructor in the Cylinder if the value of the radius and length are same as expected values.
     */
    @Test
    public void testConstructor() {
        Cylinder cylinder = new Cylinder(10.0, 2.0);
        assertEquals(2.0, cylinder.getRadius(), 0.001);
        assertEquals(10.0, cylinder.getHeight(), 0.001);
    }
    /**
     * This method tests testGetVolume method by Comparing expected Volume and actual Volume.
     */
    @Test
    public void testGetVolume() {
        Cylinder cylinder = new Cylinder(2.0, 10.0);
        assertEquals(628.3185307179587, cylinder.getVolume(), 0.001);
    }
    /**
     * This method tests testGetSurfaceArea method by Comparing expected surface area and actual surface area.
     */
    @Test
    public void testGetSurfaceArea() {
        Cylinder cylinder = new Cylinder(2.0, 10.0);
        //Comparing expectedSurfaceArea and actualSurfaceArea
        assertEquals(753.9822368615504, cylinder.getSurfaceArea(), 0.001);
    }
}
