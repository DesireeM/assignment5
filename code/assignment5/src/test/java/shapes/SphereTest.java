/**
 * @author Desiree Mariano
 * @studentid 2236344
 * @version 10/6/2023
 */

package shapes;

import static org.junit.Assert.*;

import org.junit.Test;

public class SphereTest {
    @Test
    public void shouldGiveTheConstructor() {
        Sphere s = new Sphere(2);
        assertEquals(2.0, s.getRadius(), 0.0001);
    }

    @Test
    public void shouldGiveTheVolume() {
        Sphere s = new Sphere(2);
        assertEquals(25.132741228718345, s.getVolume(), 0.0001);        
    }

    @Test
    public void shouldGiveTheArea() {
        Sphere s = new Sphere(2);
        assertEquals(50.26548245743669, s.getSurfaceArea(), 0.0001);           
    }
}
