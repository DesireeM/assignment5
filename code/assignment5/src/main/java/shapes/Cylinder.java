/**
 * @author Desiree Mariano
 * @studentid 2236344
 * @version 10/3/2023
 */

package shapes;
import java.lang.Math.*;

public class Cylinder implements IShape3d{

    private double height;
    private double radius;

    /**
     * this is the constructor that will hold the fields
     */
    public Cylinder (double height, double radius){
        this.height = height;
        this.radius = radius;
    }

     /**The getter method for the height
     * @return the height
     */
    public double getHeight() {
        return this.height;
    }

    
    /**The getter method for the radius
     * @return the radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * @return double that is the volume
     */
    public double getVolume() {
        return Math.PI * Math.pow(this.radius,2)* this.height;
    }

    /**
     * @return double that is the surface area
     */
    public double getSurfaceArea() {
        return 2* Math.PI * Math.pow(this.radius,2) + 2 * Math.PI * this.radius * this.height;
    }
}
