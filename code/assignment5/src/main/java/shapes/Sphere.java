/**
 * Skeleton is a class used for implamenting the interface Shape3d.
 * @author Sungeun Kim
 * @studentId 2042714
 * @version 10/3/2023
 */

package shapes;

import java.lang.Math.*;

public class Sphere implements IShape3d {

    private double radius;

    /**
     * This is constructor
     */
    public Sphere (double radius) {
        this.radius = radius;
    }

    /**
     * Get the radius of a shape
     * @return radius of sphere
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * Get the volume of a shape
     * @return volume of sphere
     */
    public double getVolume() {
        return Math.PI * Math.pow(this.radius, 3);
    }
    /**
     * Get the area of a shape
     * @return area of sphere
     */
    public double getSurfaceArea() {
        return 4 * Math.PI * Math.pow(this.radius, 2);
    }
}
